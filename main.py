import os
import glob
import json

path = os.getcwd()  # current working directory
counter = 0  # counting the number of files

with open('filenames_kollektivavtal.txt', 'w') as g:  # open txt file to write filenames to
    for filename in glob.glob(os.path.join(path, '*.json')):  # checking all json files
        with open(filename, encoding='utf-8', mode='r') as f:
            data = f.read()  # read data
            annotations = json.loads(data)["annotations"]  # check the correct section in json file
            for concept in annotations:
                if concept["preferred-label"] == "Kollektivavtal":
                    targets = os.path.basename(filename)  # save filename
                    print(targets)  # filename
                    g.write(targets)  # write to file
                    g.write("\n")  # add new line
                    counter += 1  # number of files found

print(f'{counter} files are annotated with "Kollektivavtal".')
